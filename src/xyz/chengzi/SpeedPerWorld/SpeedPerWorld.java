package xyz.chengzi.SpeedPerWorld;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import xyz.chengzi.SpeedPerWorld.Updater.UpdateResult;
import xyz.chengzi.SpeedPerWorld.Updater.UpdateType;

public class SpeedPerWorld extends JavaPlugin
{
    public static final Map<String, Double> walkSpeedMap = new HashMap<String, Double>();
    public static final Map<String, Double> flySpeedMap = new HashMap<String, Double>();
    
    private static final Logger log = Bukkit.getLogger();
    
    @Override
    public void onEnable()
    {
        this.saveDefaultConfig();
        
        if (this.getConfig().getBoolean("enableMetrics", true))
            try
            {
                MetricsLite metrics = new MetricsLite(this);
                if (metrics.isOptOut())
                {
                    metrics = null;
                }
                else
                {
                    metrics.start();
                    log.info("Thank you for enabling Metrics!");
                }
            }
            catch (IOException e)
            {
                log.warning("Unable to start Metrics service.");
            }
        
        if (this.getConfig().getBoolean("enableUpdater", true))
        {
            final SpeedPerWorld plugin = this;
            Runnable updateChecker = new Runnable()
            {
                @Override
                public void run()
                {
                    Updater updater = new Updater(plugin, 82800, plugin.getFile(),
                            UpdateType.NO_DOWNLOAD, true);
                    if (updater.getResult() == UpdateResult.UPDATE_AVAILABLE)
                    {
                        log.info("New version " + updater.getLatestName().split(" ")[1]
                                + " for " + updater.getLatestGameVersion()
                                + " is available now!");
                    }
                }
            };
            Bukkit.getScheduler().runTaskAsynchronously(this, updateChecker);
        }
        
        ConfigurationSection worldsSection = this.getConfig().getConfigurationSection(
                "Worlds");
        
        for (String worldName : worldsSection.getKeys(false))
        {
            ConfigurationSection worldSection = worldsSection
                    .getConfigurationSection(worldName);
            
            walkSpeedMap.put(worldName, worldSection.getDouble("walkSpeed", 1.0));
            flySpeedMap.put(worldName, worldSection.getDouble("flySpeed", 1.0));
        }
        
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
    }
}
