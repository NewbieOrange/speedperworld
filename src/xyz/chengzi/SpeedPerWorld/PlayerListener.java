package xyz.chengzi.SpeedPerWorld;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerListener implements Listener
{
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        setSpeed(event.getPlayer());
    }
    
    // I am not sure that this is needed...
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerRespawn(PlayerRespawnEvent event)
    {
        setSpeed(event.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event)
    {
        setSpeed(event.getPlayer());
    }
    
    private static void setSpeed(Player p)
    {
        Double walkSpeed = SpeedPerWorld.walkSpeedMap.get(p.getWorld().getName());
        Double flySpeed = SpeedPerWorld.flySpeedMap.get(p.getWorld().getName());
        
        p.setWalkSpeed(walkSpeed == null ? 1.0F : walkSpeed.floatValue());
        p.setFlySpeed(flySpeed == null ? 1.0F : flySpeed.floatValue());
    }
}
